import { useSelector, useDispatch } from 'react-redux';
import { ingredientAdded } from '../../actions/ingredientActions';

function Home() {

    const ingt = useSelector(state => state.ingredient)
    const dispatch = useDispatch();

    return (
        <div>
            <h1>Home</h1>
            <button onClick={() => dispatch(ingredientAdded("Testy test"))}>Klikk her!</button>
            <p></p>
        </div>
    )
}

export default Home;