import * as actions from '../actions/actionTypes'

let lastId = 0;

// Initial state to avoid undefined.
export function ingredientsReducer(state = [], action) {
    switch (action.type) {
        case actions.INGREDIENT_ADDED:
            return [
                ...state,
                {
                    id: ++lastId,
                    description: action.payload.description
                }
            ];
        case actions.INGREDIENT_REMOVED:
            return state.filter(ingredient => ingredient.id !== action.payload.id);
        default:
            return state;
    }
}