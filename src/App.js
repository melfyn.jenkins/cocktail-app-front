import 'bootstrap/dist/css/bootstrap.min.css';
import NavbarComponent from './components/Shared/NavbarComponent';
import Home from './components/Home/Home'
import About from './components/About/About'
import Settings from './components/Settings/Settings'
import {
  BrowserRouter as Router,
  Routes,
  Route
} from "react-router-dom";

function App() {
  return (
    <div className="App">
      <>
        <Router>
          <NavbarComponent />
          <Routes>
            <Route path="/" element={<Home />} />
            <Route path="/about" element={<About />} />
            <Route path="/settings" element={<Settings />} />
          </Routes>
        </Router>

      </>
    </div>
  );
}

export default App;
