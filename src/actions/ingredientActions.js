import * as actions from './actionTypes'

export const ingredientAdded = desc => ({
    type: actions.INGREDIENT_ADDED,
    payload: {
        description: desc
    }
})

